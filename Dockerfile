#####################
###### Links ########
#####################

# Tutorial create a docker file : https://kevalnagda.github.io/dockerfile
# Ansible docker image repository: https://github.com/William-Yeh/docker-ansible
# Docker brocade repository: https://github.com/brocade/ansible

#####################
####### Run #########
#####################

# Just execute command "docker build ." in current Dockerfile directory and that's all folks. 

#####################
### Configuration ###
#####################

# import Docker base file
# NB: choose here wanted distribution
FROM williamyeh/ansible:debian9-onbuild

# ==> Specify requirements filename;  default = "requirements.yml"
# NB: Change here your requirements.yml pathfile (inside project)
ENV REQUIREMENTS  requirements.yml

# ==> Specify playbook filename;      default = "playbook.yml"
# NB: Change here your playbook.yml pathfile (inside project)
ENV PLAYBOOK      playbook.yml

# ==> Specify inventory filename;     default = "/etc/ansible/hosts"
# NB: Change here your inventory.ini pathfile (inside project)
ENV INVENTORY     inventory.ini

# installing brocade library
# Maybe verify copy of ansible.cfg (if dest path is good or not and check if content is ok with brocade,
# not sure => taken instructions from brocade repo)

RUN apt add git && \
	git clone https://github.com/brocade/ansible /tmp/brocade_ansible && \
	export ANSIBLE_LIBRARY="/tmp/brocade_ansible/library" && \ 
	cp /tmp/brocade_ansible/ansible.cfg ./ansible.cfg 

# ==> Executing Ansible (with a simple wrapper)...

RUN ansible-playbook-wrapper
